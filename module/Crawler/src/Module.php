<?php
/**
 * @link      https://gitlab.com/anindya23/zf3sk.git for the source repository
 * @copyright Copyright (c) 2005 - 2016 by InfoSource Ltd.
 */

namespace Crawler;

class Module
{

    /**
     * Returns module's config file
     * 
     * @return module.config.php
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
