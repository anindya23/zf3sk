<?php
/**
 * @link      https://gitlab.com/anindya23/zf3sk.git for the source repository
 * @copyright Copyright (c) 2005 - 2016 by InfoSource Ltd.
 */

namespace Crawler\Service;

/**
 * Util service provides util related functionalities to the CrawlerManager
 * service in the application
 *
 * @author Anindya Roy Chowdhury<anindya23@gmail.com>
 */
class UtilService
{

    /**
     * Facebook domain for the comparison in Util Service's methods
     */
    const FACEBOOK = "facebook.com";

    /**
     * Linkedin domain for the comparison in Util Service's methods
     */
    const LINGEDIN = "linkedin.com";

    /**
     * Twitter domain for the comparison in Util Service's methods
     */
    const TWITTER = "twitter.com";

    /**
     * Youtube domain for the comparison in Util Service's methods
     */
    const YOUTUBE = "youtube.com";

    /**
     * Regex pattern to validate the facebook page
     */
    const FACEBOOK_PATTERN = '/^(http[s]?:\/\/)?((www|[a-zA-Z]{2}-[a-zA-Z]{2})\.)?facebook\.com\/(pages\/[a-zA-Z0-9\.-]+\/[0-9]+|[a-zA-Z0-9\.-]+)\b[\/]?/miuU';

    /**
     * Regex pattern to validate the linkedin page
     */
    const LINKEDIN_PATTERN = '/^(http(s)?:\/\/)?([\w]+\.)?linkedin\.com\/(pub|in|profile|company)/';

    /**
     * Regex pattern to validate the twitter page
     */
    const TWITTER_PATTERN = '/^(http[s]?:\/\/)?((www|[a-zA-Z]{2}-[a-zA-Z]{2})\.)?twitter\.com\/[A-Za-z0-9_]+$/';

    /**
     * Regex pattern to validate the youtube page
     */
    const YOUTUBE_PATTERN = '/^(http[s]?:\/\/)?((www|[a-zA-Z]{2}-[a-zA-Z]{2})\.)?youtube\.com\/(channel\/|user\/)[a-zA-Z0-9\-]+$/';

    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Constructor. Doctrine's entity manager injected through UtilServiceFactory
     */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Check whether the provided url is Facebook page or not
     * 
     * @param string $url
     * @return boolean
     */
    public function isFacebookPage($url)
    {
        return preg_match_all(self::FACEBOOK_PATTERN, $url, $matches) ? true : false;
    }

    /**
     * Check whether the provided url is LinkedIn page or not
     * 
     * @param string $url
     * @return boolean
     */
    public function isLinkedInPage($url)
    {
        return preg_match_all(self::LINKEDIN_PATTERN, $url, $matches) ? true : false;
    }

    /**
     * Check whether the provided url is Twitter page or not
     * 
     * @param string $url
     * @return boolean
     */
    public function isTwitterPage($url)
    {
        return preg_match_all(self::TWITTER_PATTERN, $url, $matches) ? true : false;
    }

    /**
     * Check whether the provided url is Youtube page or not
     * 
     * @param string $url
     * @return boolean
     */
    public function isYoutubePage($url)
    {
        return preg_match_all(self::YOUTUBE_PATTERN, $url, $matches) ? true : false;
    }

    /**
     * 
     * @param String $url
     * @return string|boolean
     */
    public function getDomain($url)
    {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : '';

        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            return $regs['domain'];
        }
        return false;
    }

    /**
     * Check whether the provided url is same of crawling url or not
     * 
     * @param string $url
     * @param string $companyUrl
     * @return boolean
     */
    public function isCompanyDomain($url, $companyUrl)
    {
        $pieces = parse_url($url);
        $domain = isset($pieces['host']) ? $pieces['host'] : '';

        if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
            if ($regs['domain'] == $this->getDomain($companyUrl)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check both urls are same or not
     * 
     * @param string $url1
     * @param string $url2
     * @return boolean
     */
    public function isSameUrl($url1, $url2)
    {
        $mustMatch = array_flip(['host', 'port', 'path']);
        $defaults = ['path' => '/'];
        $url1 = array_intersect_key(parse_url($url1), $mustMatch) + $defaults;
        $url2 = array_intersect_key(parse_url($url2), $mustMatch) + $defaults;

        return $url1 === $url2;
    }

    /**
     * Check URL if it's a Social Media URL or not
     * 
     * @param String $url
     * @return boolean
     */
    public function isValidSocialMedia($url)
    {
        $status = false;
        $domain = $this->getDomain($url);

        switch ($domain) {
            case self::FACEBOOK:
                $status = $this->isFacebookPage($url);
                break;
            case self::LINGEDIN:
                $status = $this->isLinkedInPage($url);
                break;
            case self::TWITTER:
                $status = $this->isTwitterPage($url);
                break;
            case self::YOUTUBE:
                $status = $this->isYoutubePage($url);
                break;
            default:
                $status = false;
                break;
        }
        return $domain;
    }
}
