<?php
/**
 * @link      https://gitlab.com/anindya23/zf3sk.git for the source repository
 * @copyright Copyright (c) 2005 - 2016 by InfoSource Ltd.
 */

namespace Crawler\Service;

use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

/**
 * CrawlerManager if the core class to provide crawling functionalities to the 
 * application. 
 *
 * @author Anindya Roy Chowdhury<anindya23@gmail.com>
 */
class CrawlerManager
{

    /**
     * Facebook domain for the comparison in Util Service's methods
     */
    const FACEBOOK = "facebook.com";

    /**
     * Linkedin domain for the comparison in Util Service's methods
     */
    const LINGEDIN = "linkedin.com";

    /**
     * Twitter domain for the comparison in Util Service's methods
     */
    const TWITTER = "twitter.com";

    /**
     * Youtube domain for the comparison in Util Service's methods
     */
    const YOUTUBE = "youtube.com";

    /**
     * Default time till the crawler crawls the url in a recursive way.
     */
    const TIMEOUT = 120;

    /**
     * Doctrine entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * CrawlerManager uses UtilService for the util functionalities
     * 
     * @var Crawler\Service\UtilService
     */
    private $utilService;

    /**
     * CrawlerManager uses Goutte Library for crawling. It wraps popular PHP 
     * Crawling library GuzzleHttp. The variable holds the Client object of 
     * Goutte Library
     * 
     * @var Goutte\Client 
     */
    private $client;

    /**
     * The array contains the Social Medias Links retrieved by crawling
     * 
     * @var array 
     */
    private $socialMediaList = array(
        'facebook' => '',
        'linkedIn' => '',
        'twitter' => '',
        'youtube' => ''
    );

    /**
     * Constructor. Doctrine's EntityManager and UtilService injected through 
     * CrawlerManagerFactory
     */
    public function __construct($entityManager, $utilService)
    {
        $this->entityManager = $entityManager;
        $this->utilService = $utilService;
    }

    /**
     * Initialize Crawler Client
     * 
     * @return Goutte\Client
     */
    public function setClient()
    {
        $this->client = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => self::TIMEOUT
        ));
        $this->client->setClient($guzzleClient);
        return $this;
    }

    /**
     * Get Crawler Client
     * 
     * @return Goutte\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Traverse URL and retrieve Social Media URLs and populate array with 
     * Social Media Links and return that array
     * 
     * @param Goutte\Client $client
     * @param String $url
     * @param Integer $depth
     * @return array
     */
    public function getSocialMediaList($url, $depth = 1)
    {
        $companyUrl = $url;
        $this->setClient();

        $depthList[] = array($url);

        for ($i = 0; $i < $depth; $i++) {
            foreach ($depthList[$i] as $page) {
                $crawler = $this->client->request('GET', $page);

                $crawler->filter('a')->each(function ($node) use ($i, $companyUrl) {
                    $url = $node->attr('href');

                    if ($this->utilService->isValidSocialMedia($url)) {
                        $this->setSocialMediaList($url);
                    } else if ($this->utilService->isCompanyDomain($url, $companyUrl) && !in_array($url, $depthList[$i + 1]) && !$this->utilService->isSameUrl($url, $companyUrl)) {
                        $depthList[$i + 1][] = $url;
                    }
                });
            }
        }
        return $this->socialMediaList;
    }

    /**
     * Validate the url and populate key with the correspondin URL
     * 
     * @param String $url
     */
    public function setSocialMediaList($url)
    {
        $domain = $this->utilService->getDomain($url);

        switch ($domain) {
            case self::FACEBOOK:
                if (empty($this->socialMediaList['facebook'])) {
                    $this->socialMediaList['facebook'] = $url;
                }
                break;
            case self::LINGEDIN:
                if (empty($this->socialMediaList['linkedIn'])) {
                    $this->socialMediaList['linkedIn'] = $url;
                }
                break;
            case self::TWITTER:
                if (empty($this->socialMediaList['twitter'])) {
                    $this->socialMediaList['twitter'] = $url;
                }
                break;
            case self::YOUTUBE:
                if (empty($this->socialMediaList['youtube'])) {
                    $this->socialMediaList['youtube'] = $url;
                }
                break;
        }
    }
}
