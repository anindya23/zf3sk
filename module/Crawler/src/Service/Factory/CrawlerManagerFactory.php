<?php
/**
 * @link      https://gitlab.com/anindya23/zf3sk.git for the source repository
 * @copyright Copyright (c) 2005 - 2016 by InfoSource Ltd.
 */

namespace Crawler\Service\Factory;

use Interop\Container\ContainerInterface;
use Crawler\Service\CrawlerManager;
use Crawler\Service\UtilService;

/**
 * Creates and injects required classes to CrawlerManager
 *
 * @author Anindya Roy Chowdhury<anindya23@gmail.com>
 */
class CrawlerManagerFactory
{

    /**
     * This method configures and injects Doctrine's EntityManager and 
     * UtilService classes and returns CrawlerManager's object. 
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $utilService = $container->get(UtilService::class);

        return new CrawlerManager($entityManager, $utilService);
    }
}
