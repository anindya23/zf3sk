<?php
/**
 * @link      https://gitlab.com/anindya23/zf3sk.git for the source repository
 * @copyright Copyright (c) 2005 - 2016 by InfoSource Ltd.
 */

namespace Crawler\Service\Factory;

use Interop\Container\ContainerInterface;
use Crawler\Service\UtilService;

/**
 * Creates and injects required classes to UtilService
 *
 * @author Anindya Roy Chowdhury<anindya23@gmail.com>
 */
class UtilServiceFactory
{

    /**
     * This method creates the UtilManager service and returns its instance. 
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new UtilService($entityManager);
    }
}
