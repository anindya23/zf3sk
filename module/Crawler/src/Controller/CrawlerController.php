<?php
/**
 * @link      https://gitlab.com/anindya23/zf3sk.git for the source repository
 * @copyright Copyright (c) 2005 - 2016 by InfoSource Ltd.
 */

namespace Crawler\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * CrawlerController will be responsible to manage crawling functionalities of
 * Simple Crawling Application.
 *
 * @author Anindya Roy Chowdhury<anindya23@gmail.com>
 */
class CrawlerController extends AbstractActionController
{

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Crawler manager.
     * @var Crawler\Service\CrawlerManager 
     */
    private $crawlerManager;

    /**
     * Constructor. Doctrine's entity manager and CrawlerManager injected 
     * through Controller factory
     */
    public function __construct($entityManager, $crawlerManager)
    {
        $this->entityManager = $entityManager;
        $this->crawlerManager = $crawlerManager;
    }

    /**
     * Default action of the controller.
     * 
     * @return ViewModel
     */
    public function indexAction()
    {
        $crawlData = null;
        $errorMessage = '';
        
        if ($this->getRequest()->isPost()) {

            $data = $this->params()->fromPost();
            
            if (filter_var($data['url'], FILTER_VALIDATE_URL)) {
                $crawlData = $this->crawlerManager
                        ->getSocialMediaList($data['url'], 1);
            } else {
                $errorMessage = "URL is not valid";
            }
        }
        return new ViewModel([
            'crawlData' => $crawlData,
            'errorMessage' => $errorMessage
        ]);
    }
}
