<?php
/**
 * @link      https://gitlab.com/anindya23/zf3sk.git for the source repository
 * @copyright Copyright (c) 2005 - 2016 by InfoSource Ltd.
 */

namespace Crawler\Controller\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Crawler\Controller\CrawlerController;
use Crawler\Service\CrawlerManager;

/**
 * CrawlerControllerFactory injects Doctrine's EntityManager and CrawlerManager
 * service to the controller.
 *
 * @author Anindya Roy Chowdhury<anindya23@gmail.com>
 */
class CrawlerControllerFactory implements FactoryInterface
{

    /**
     * Invoke method creates and configures the objects those are to be injected
     * to the CrawlerController class
     * 
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array $options
     * @return Crawler\Controller\CrawlerController
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $crawlerManager = $container->get(CrawlerManager::class);

        return new CrawlerController($entityManager, $crawlerManager);
    }
}
