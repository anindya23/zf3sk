<?php

namespace Crawler;

use Zend\ServiceManager\Factory\InvokableFactory;
use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

return [
    'controllers' => [
        'factories' => [
            //Controller\CrawlerController::class => InvokableFactory::class,
            Controller\CrawlerController::class => Controller\Factory\CrawlerControllerFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'crawler' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/crawler',
                    'defaults' => [
                        'controller' => Controller\CrawlerController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\UtilService::class => Service\Factory\UtilServiceFactory::class,
            Service\CrawlerManager::class => Service\Factory\CrawlerManagerFactory::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'Crawler' => __DIR__ . '/../view',
        ],
    ],
];

