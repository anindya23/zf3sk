<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace CrawlerTest\Controller;

use Crawler\Controller\CrawlerController;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

/**
 * Description of CrawlerTest
 *
 * @author anindya
 */
class CrawlerControllerTest extends AbstractHttpControllerTestCase
{

    public function setUp()
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            $configOverrides
        ));

        parent::setUp();
    }
    
    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch('/crawler', 'GET');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('crawler');
        $this->assertControllerName(CrawlerController::class); // as specified in router's controller name alias
        $this->assertControllerClass('CrawlerController');
        $this->assertMatchedRouteName('crawler');
    }
}
